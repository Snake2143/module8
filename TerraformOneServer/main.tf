terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = "y0_AgAAAABZHAn2AATuwQAAAAD0j2CC4Ra7u29NTXi8nYJFiOHsWSKJZ9g"
  cloud_id  = "b1g4gnb9ic202k1a3o0n"
  folder_id = "b1grqv94v68g4evp12iq"
  zone      = "ru-central1-b"
}

data "yandex_compute_image" "my-ubuntu-2004-1" {
  family = "ubuntu-2004-lts"
}

resource "yandex_compute_instance" "my-vm-1" {
  name        = "test-vm-1"
  platform_id = "standard-v1"
  zone        = "ru-central1-b"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my-ubuntu-2004-1.id}"
    }
  }

  network_interface {
    subnet_id = "e2liid8e7rs52kg24r5g"
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/test.pub")}"
  }
}

#resource "yandex_vpc_network" "my-nw-1" {
#  name = "my-nw-1"
#}

resource "yandex_vpc_subnet" "my-nw-2" {
  zone           = "ru-central1-b"
  network_id     = "e2liid8e7rs52kg24r5g"
  v4_cidr_blocks = ["192.168.10.0/24"]
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.my-vm-1.network_interface.0.ip_address
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.my-vm-1.network_interface.0.nat_ip_address
}
